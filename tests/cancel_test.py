from concurrentevents import catch, EventManager, cancel

order = []


def test_cancel():
    order = []

    event_manager = EventManager()

    event_manager.add_handlers(catch('Start', 1)(lambda: order.append(1)))
    event_manager.add_handlers(catch('Start', 2)(lambda: order.append(2)))
    event_manager.add_handlers(catch('Start', 3)(lambda: cancel()))
    event_manager.add_handlers(catch('Start', 4)(lambda: order.append(3)))

    event_manager.start()

    assert order == [1, 2]
