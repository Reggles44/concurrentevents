import statistics
import time

from concurrentevents import catch, ResourcePool, Resource, EventManager

test_num = 20
pool_size = 2
timeout = 1
sleep = 5


class Incrementer(Resource):
    def __init__(self):
        self.count = 0
        super().__init__(f=self.increment)

    def increment(self):
        self.count += 1


resource_pool = ResourcePool(resource=Incrementer(), amount=pool_size, thread_safe=True)


@catch("ResourceEvent")
def resource_increment():
    with resource_pool.get(timeout=timeout) as res:
        if res.count == (test_num / pool_size) - 2:
            res()
            time.sleep(sleep)
            raise Exception
        else:
            res.increment()


def test_resource_pool():
    event_manager = EventManager()
    event_manager.add_handlers(catch('Start')(lambda: [event_manager.fire('ResourceEvent') for _ in range(test_num)]))
    event_manager.add_handlers(resource_increment)
    event_manager.start()

    counts = [resource_pool.get().count for _ in range(pool_size)]

    assert sum(counts) == test_num - pool_size
    assert statistics.variance(counts) < 1
