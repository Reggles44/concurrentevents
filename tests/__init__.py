import logging

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('concurrentevents').setLevel(logging.DEBUG)
