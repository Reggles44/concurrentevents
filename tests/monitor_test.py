import time

from concurrentevents import catch, EventManager, thread_monitor
from concurrentevents.tools.threadmonitor import thread_monitoring


@catch("MonitorEvent")
@thread_monitor
def foo():
    with thread_monitor('UNIQUEKEY'):
        time.sleep(2)


def test_monitor():
    event_manager = EventManager()
    event_manager.add_handlers(catch('Start')(lambda: [event_manager.fire('MonitorEvent') for _ in range(5)]))
    event_manager.add_handlers(foo)
    event_manager.start()

    print(set(thread_monitoring.keys()))

    assert 'UNIQUEKEY' in thread_monitoring
    assert 'monitor_test.foo()' in thread_monitoring
