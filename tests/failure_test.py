import time

from concurrentevents import catch, ResourcePool, Resource, EventManager, Priority


def makes_exception(func, *args, **kwargs):
    try:
        func(*args, **kwargs)
        return False
    except Exception as e:
        print(e)
        return True


class FailureHandler:
    incorrect_failure = False

    @catch("Start", priority=Priority.END)
    def start_failing(self):
        event_manager._submit(f=lambda: print("Hello World"))
        self.fire("FailureEvent")

    @catch("FailureEvent")
    def invalid_stuff(self):
        assert makes_exception(self.fire, "EventNoHandler")
        assert makes_exception(self.fire, 'Start')

        self.fire("Exit", timeout=0)

        raise Exception("This should be fired")

    @catch("Exit")
    def timeout_exit(self):
        time.sleep(5)
        print("This should be timed out")


def test_failure():
    event_manager = EventManager()

    assert makes_exception(EventManager, threads="I want a scarf")
    assert makes_exception(EventManager, threads=-5)
    assert makes_exception(event_manager.add_handlers, 4)
    assert makes_exception(event_manager.fire, 11)
    assert makes_exception(catch, event=None)
    assert makes_exception(catch, event=4)
    assert makes_exception(catch, event="Event", priority=None)
    assert makes_exception(ResourcePool, resource="Not a resource", amount=5)
    assert makes_exception(ResourcePool, resource=Resource(f=lambda a: a + 1), amount="I want Five")
    assert makes_exception(ResourcePool, resource=Resource(f=lambda a: a + 1), amount=5, thread_safe="Safe Please")

    res_pool = ResourcePool(resource=Resource(f=lambda a: a + 1), amount=5, thread_safe=True)
    assert makes_exception(res_pool.get, timeout="Never")
    assert makes_exception(res_pool.get, timeout=-5)

    assert makes_exception(event_manager.fire, 'Start')
    assert makes_exception(event_manager.start)
    event_manager.add_handlers(catch('Start')(lambda: None))
    failure_handler = FailureHandler()
    event_manager.add_handlers(failure_handler)
    event_manager.start()
    assert makes_exception(event_manager.start)

    assert not failure_handler.incorrect_failure
