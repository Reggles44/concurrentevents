from concurrentevents import catch, EventManager

bulk_amount = 200
count = 0


@catch('BulkEvent')
def increment():
    global count
    count += 1


def test_bulk():
    event_manager = EventManager()
    event_manager.add_handlers(catch('Start')(lambda: [event_manager.fire('BulkEvent') for _ in range(bulk_amount)]))
    event_manager.add_handlers(increment)
    event_manager.start()
    assert count == bulk_amount
