from concurrentevents import EventManager, catch, Priority


def test_priority():
    order = []

    event_manager = EventManager()

    event_manager.add_handlers(catch('Start', Priority.END)(lambda: order.append(0)))
    event_manager.add_handlers(catch('Start', Priority.LOW)(lambda: order.append(1)))
    event_manager.add_handlers(catch('Start', Priority.MEDIUM)(lambda: order.append(2)))
    event_manager.add_handlers(catch('Start', Priority.HIGH)(lambda: order.append(3)))
    event_manager.add_handlers(catch('Start', Priority.CRITICAL)(lambda: order.append(4)))

    event_manager.start()

    assert set(order) - set(range(0, 6)) == set()
