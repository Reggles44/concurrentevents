import time

from concurrentevents import catch, EventManager


def test_exit():
    order = set()
    exit_bool = []

    event_manager = EventManager()
    event_manager.add_handlers(catch("Start")(lambda: [event_manager.fire('ExitTest', i) for i in range(10)]))
    event_manager.add_handlers(catch('Exit')(lambda: exit_bool.append(True)))

    @catch('ExitTest')
    def foo(i):
        if i == 3:
            time.sleep(10)
        elif i == 4:
            event_manager.fire('Exit', timeout=30)
        else:
            order.add(i)

    event_manager.add_handlers(foo)
    event_manager.start()

    assert set(order) == {i for i in range(10)} - {3, 4}
    assert sum(exit_bool)


def test_exit_no_timeout():
    exit_bool = []

    event_manager = EventManager()
    event_manager.add_handlers(catch("Start")(lambda: event_manager.fire('Exit')))
    event_manager.add_handlers(catch('Exit')(lambda: exit_bool.append(True)))

    event_manager.start()

    assert sum(exit_bool)
