from concurrentevents import catch, EventManager

test_string = "Hello World"


class TestClass:
    @catch('Start')
    def foo(self):
        print(test_string)


def test_base(capfd):
    event_manager = EventManager()
    event_manager.add_handlers(TestClass())
    event_manager.start()

    out, err = capfd.readouterr()
    assert out.strip() == test_string
