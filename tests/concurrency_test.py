import time

from concurrentevents import catch, EventManager, thread_monitor

concurrent_amount = 16
count = 0


@catch("ConcurrentEvent")
@thread_monitor
def clean_pass():
    global count
    count += 1
    if count == concurrent_amount - 1:
        time.sleep(0.5)
        raise Exception


def test_concurrency():
    event_manager = EventManager()

    event_manager.add_handlers(
        catch('Start')(lambda: [event_manager.fire('ConcurrentEvent') for _ in range(concurrent_amount)]))
    event_manager.add_handlers(clean_pass)

    event_manager.start()
    assert count == concurrent_amount
