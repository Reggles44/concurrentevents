from concurrentevents import catch, EventManager


def test_shutdown():
    count = []

    event_manager = EventManager(threads=1)

    @catch('Start')
    def foo():
        event_manager.fire('one')
        exit()
        event_manager.fire('two')
        event_manager.fire('three')

    event_manager.add_handlers(foo)

    event_manager.add_handlers(catch('one')(lambda: count.append(1)))
    event_manager.add_handlers(catch('two')(lambda: count.append(2)))
    event_manager.add_handlers(catch('three')(lambda: count.append(3)))


    event_manager.start()

    assert set(count) == {1}
