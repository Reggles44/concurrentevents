Resource Pool
=============
A example implementation of the resource pool tool to do round-robin distribution of a resource to threads.
In this particular example the Incrementer class is serving as our resources that will be distributed.
Instead of a single instance the pool will store three different instances that will be evenly distributed.
Specifically, in this example, each instance of the incrementer class will end up with a count of 20 as there will be 60 total events and 3 incrementer instances.

.. literalinclude:: ../../../examples/resourcepool.py

