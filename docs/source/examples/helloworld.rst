Hello World
===========
The simplest implementation of the event system using only the basic of tools to run code from the Start event.
This implementation does not use the ```ConcurrentEvents.EventHandler``` superclass as the additional functionality isn't used.
However it is recommended to use the ```ConcurrentEvents.EventHandler``` for any firing and canceling of events as it standardizes that functionality.

.. literalinclude:: ../../../examples/helloworld.py
