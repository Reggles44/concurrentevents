Ping Pong
===========
In this example we create a Ping and Pong event which are simply there to distinguish handling functions.
No custom __init__ function is requried for these event classes because the Event __init__ function is completely generic and will hold any arguments or keyword arguments without issue.

Then by Catch those events to corresponding events and firing the opposite event we can bounce between the two functions.
Eventually this loop does end as the int that is being passed between the two events is incrementing and our exit condition is reached.

When the Exit event is fired no other events can be fired or will be handled and the main cleaning loop of the event system will exit.


.. literalinclude:: ../../../examples/pingpong.py

