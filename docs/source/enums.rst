Enums
=================

Priority
--------

.. automodule:: concurrentevents.enums.Priority
   :members:
   :undoc-members:
   :show-inheritance:
