.. ConcurrentEvents documentation master file, created by
sphinx-quickstart on Fri Nov  1 14:52:38 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

.. include:: ../../README.rst

Documentation
=============

.. toctree::
   :maxdepth: 2
   :caption: General:

   core
   tools
   enums

.. toctree::
   :maxdepth: 1
   :caption: Examples:

   examples/helloworld
   examples/kwargs
   examples/pingpong
   examples/resourcepool


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
