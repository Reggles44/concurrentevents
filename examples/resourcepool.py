from concurrentevents import Resource, EventHandler, catch, ResourcePool, EventManager


class Incrementer(Resource):
    def __init__(self):
        super().__init__()
        self.count = 0

    def increment(self):
        self.count += 1


class Handler(EventHandler):

    @catch('Start')
    def start(self):
        for i in range(0, 60):
            self.fire('TestEvent')

    @catch('TestEvent')
    def inc(self):
        with pool.get() as res:
            res.increment()


event_manager = EventManager()
event_manager.add_handlers(Handler())
pool = ResourcePool(resource=Incrementer(), amount=3)
event_manager.start()
