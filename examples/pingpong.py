from concurrentevents import catch, EventManager


class PingPong:

    @catch("Start")
    def start(self):
        self.fire('Ping', 1)

    @catch("Ping")
    def ping(self, count):
        print("Ping {}".format(count))
        self.fire('Pong', count + 1)

    @catch("Pong")
    def pong(self, count):
        print("Pong {}".format(count))
        if count >= 50:
            self.fire('Exit')
        else:
            self.fire('Ping', count + 1)


event_manager = EventManager()
event_manager.add_handlers(PingPong)
event_manager.start()
