from concurrentevents import EventManager, catch


@catch("Start")
def helloWorld():
    print('Hello World')


event_manager = EventManager()
event_manager.add_handlers(helloWorld)
event_manager.start()
