cd tests
python -m pip install --upgrade pytest coverage requests
coverage run --source=concurrentevents -m pytest
coverage report -m
cd ..
